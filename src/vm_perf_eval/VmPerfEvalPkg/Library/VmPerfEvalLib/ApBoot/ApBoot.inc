; Copyright 2022 The ChromiumOS Authors
; Use of this source code is governed by a BSD-style license that can be
; found in the LICENSE file.
;

;General purpose register array follows the encoding
;RAX, RCX, RDX, RBX, RSP, RBP, RSI, RDI, R8 - R15

;Private Block Layout (32KiB):
;+0000h: GDT (Give 512 x 8 bytes) => 4K
;+1000h: IDT (256 * 8/16 bytes) => 4K
;+2000h: TSS (needed for long mode) = 256
;+2100h: IOPM (IO Permissions) => 8KiB
;+4100h: Int jump table (256 * 8 bytes) => 2KiB
;+4900h: Int Counter table (256 * 4 bytes) => 1KiB
;+4D00h: Scratch (1KiB)
;+5100h: End of table
;
%define PRIV_GDT     0x0000
%define PRIV_IDT     0x1000
%define PRIV_TSS     0x2000
%define PRIV_IOPM    0x2100
%define PRIV_INTJMP  0x4100
%define PRIV_INTCNT  0x4900
%define PRIV_SCRATCH 0x4D00

;Multiple entry point types can be specified by the system
;This value indicates 2 things to the bootstrapper:
;1. How much setup needs to be performed
;2. How to interpret the entry point

%define     ENTRY_REAL_MODE     0x0A
%define     ENTRY_PM_NO_PAGING  0x0B
%define     ENTRY_PM_PAGING     0x0C
%define     ENTRY_PM_PAE_PAGING 0x0D
%define     ENTRY_LONG_MODE     0x0E

%define GENREG_MULT   8

%define IDX_AX   0
%define IDX_CX   1
%define IDX_DX   2
%define IDX_BX   3
%define IDX_SP   4
%define IDX_BP   5
%define IDX_SI   6
%define IDX_DI   7
%define IDX_R8   8
%define IDX_R9   9
%define IDX_R10  10
%define IDX_R11  11
%define IDX_R12  12
%define IDX_R13  13
%define IDX_R14  14
%define IDX_R15  15

;If the MSB is set, the processor has halted itself
;The various codes in the upper 4-bits will indicate
;WHY it has been halted.
;The lower bits can include addtional information
;The exception codes contain the exception index
%define STATUSCODE_IN_ENTRY_POINT                0x60000000
%define STATUSCODE_TRIGGER_WAIT                  0x70000000
%define STATUSCODE_SUCCESSFUL_RETURN             0x80000000
%define STATUSCODE_EXCEPTION_REAL_MODE           0x90000000
%define STATUSCODE_EXCEPTION_PROTECTED_MODE      0xA0000000
%define STATUSCODE_EXCEPTION_LONG_MODE           0xB0000000

;A value used to isolate only the status code from the value
%define STATUSCODE_MASK                          0xF0000000

struc PCIB

 ;The values below are input (they are to be set by the BSP)
 lm_pt          resd 1                  ;Long Mode Page Table (generated by BSP), physical address
 lm_stack       resd 1                  ;Stack pointer (generated by BSP), physical address
 lm_intstack    resd 1                  ;Interrupt Stack
 lm_nmistack    resd 1                  ;NMI stack
 lm_excpstack   resd 1                  ;Exception stack
 trigger_addr   resd 1                  ;Trigger address (< 1MiB)
 priv_block     resd 1                  ;32KiB block private to the CPU booting through this
 ap_buffer      resd 1                  ;A pointer to the application buffer
 ap_size        resd 1                  ;The size of the application buffer
 entry_point    resq 1                  ;AP Entry Point
 entry_type     resd 1                  ;Entry Point Type
 apic_base      resd 1                  ;32-bit base address of the LAPIC

 ;The values below are for use by the target CPU (or AP)
 statuscode:    resd 1                  ;Bootstrapper status code
 returnvalue:   resq 1                  ;Return value (if AP entry function returns)
 cr0value       resq 1                  ;CR0 value
 cr2value:      resq 1                  ;CR2 value
 excp_ds        resw 1                  ;Exception DS
 excp_es        resw 1                  ;Exception ES
 excp_ss        resw 1                  ;Exception SS
 excp_cs        resw 1                  ;Exception CS
 genregs:       resq 16                 ;General purpose register state
 errorcode:     resd 1                  ;Exception Error Code
 flagsreg:      resd 1                  ;Exception EFLAGS
 excp_rip       resq 1                  ;Exception EIP/RIP
 pcib_run_base  resd 1                  ;Run Base
endstruc
