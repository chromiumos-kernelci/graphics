#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2020 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Python package for Chrome OS graphics results database."""

from __future__ import print_function

from setuptools import setup

setup(name='results_database',
      version='1.0',
      description='Module for graphics results database tools',
      py_modules=['results_database'],
     )
