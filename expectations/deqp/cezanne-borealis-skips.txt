# cezanne/green_sardine boards: guybrush

# aligns with upstream mesa ci expectations
dEQP-VK.dynamic_rendering.basic.*

# flaky due to timeout
dEQP-VK.memory.mapping.dedicated_alloc.buffer.full.variable.implicit_unmap
dEQP-VK.memory.mapping.dedicated_alloc.image.full.variable.implicit_unmap
dEQP-VK.memory.pipeline_barrier.transfer_src_transfer_dst.1048576
dEQP-VK.texture.explicit_lod.2d.sizes.128x128_linear_linear_mipmap_linear_clamp
dEQP-VK.texture.explicit_lod.2d.sizes.128x128_linear_linear_mipmap_linear_repeat

# skip for flaky tests
dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_8_32.samples_2.d16_unorm_s8_uint.depth_zero_stencil_none_testing_stencil_samplemask
dEQP-VK.renderpass2.depth_stencil_resolve.image_2d_8_32.samples_4.d32_sfloat_s8_uint.depth_zero_stencil_min_testing_stencil_samplemask
